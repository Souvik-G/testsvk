/** @format */
import React from 'react';
import { Text } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ReduxNetworkProvider } from 'react-native-offline';
import { store, persistor } from './store';
import App from './App';

const ReduxApp = () => (
  <Provider store={store}>
    <PersistGate
      loading={<Text>loading persisted store</Text>}
      persistor={persistor}
    >
      <ReduxNetworkProvider>
        <App />
      </ReduxNetworkProvider>
    </PersistGate>
  </Provider>
);

export default ReduxApp;
