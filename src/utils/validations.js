export const validateInput = async (type, value) => {
  const regEx = {
    name: /^\s*[0-9a-zA-Z][0-9a-zA-Z '-]{2,35}$/,
    email: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
    phonenumber: /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/,
    password: '',
    passwordRe: ''
  };

  const is_valid = regEx[type] ? regEx[type].test(value) : true;

  if (!is_valid) {
    throw Error(type);
  }
  return true;
};
