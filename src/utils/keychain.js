import {
  setGenericPassword,
  getGenericPassword,
  resetGenericPassword
} from 'react-native-keychain';

export const saveToken = async token => {
  try {
    await setGenericPassword('token', token);
    return 'success';
  } catch (err) {
    throw Error(`Could not save credentials, ${err}`);
  }
};

export const loadToken = async () => {
  try {
    const { password } = await getGenericPassword();
    return password;
  } catch (err) {
    throw Error(`Could not load credentials. ${err}`);
  }
};

export const resetToken = async () => {
  try {
    await resetGenericPassword();
    return 'success!';
  } catch (err) {
    throw Error(`Could not reset credentials, ${err}`);
  }
};
