export const colorSecondary = '#4CAF50';
export const colorTextPrimary = '#2b2b2b';
export const colorTextSecondary = '#a3aebd';
export const colorSilver = '#f9f9f9';
export const colorSilverDark = '#eff1f3';
export const colorBorderSecondary = '#53a457';
export const colorWhite = '#ffffff';
