import { Dimensions } from 'react-native';

export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;
export const deviceHeightWidthMax = Math.max(deviceHeight, deviceWidth);
export const deviceHeightWidthMin = Math.min(deviceHeight, deviceWidth);
