// get keyboard type for <TextInput />
export const getKeyboardType = key => {
  switch (key) {
    case 'phonenumber':
    case 'isd_code':
    case 'pin_code':
      return 'numeric';
    case 'email':
      return 'email-address';
    default:
      return 'default';
  }
};
