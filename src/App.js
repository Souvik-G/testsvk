import React from 'react';
import { connect } from 'react-redux';
import RootNavigator from './RootNavigator';
import { Auth } from './modules/auth';

const App = ({ loggedIn }) => {
  if (!loggedIn) {
    return <Auth />;
  }

  return <RootNavigator />;
};

const mapStateToProps = state => state.auth;

export default connect(mapStateToProps)(App);
