import { loadToken } from '../utils';

export const baseUrl = 'http://api.example.com';
// Create a Memoized GET method Function-Closure that loads refresh_token only once
export const makeGET = () => {
  let token;
  return async endPoint => {
    if (!token) token = await loadToken();
    const requestUrl = baseUrl + endPoint;
    const response = await fetch(requestUrl, {
      headers: {
        Authorization: token,
        Accept: 'application/json'
      }
    });

    try {
      return await response.json();
    } catch (err) {
      throw Error(`GET Error: ${err}`);
    }
  };
};

// Create a Memoized POST method Function-Closure that loads refresh_token only once
export const makePOST = () => {
  let token;
  return async (endPoint, objData) => {
    if (!token) {
      token = await loadToken();
    }
    const requestUrl = baseUrl + endPoint;
    const formData = new FormData();
    formData.append('data', JSON.stringify(objData));
    const response = await fetch(requestUrl, {
      method: 'POST',
      headers: {
        Authorization: token,
        Accept: 'application/json'
      },
      body: formData
    });

    try {
      return await response.json();
    } catch (err) {
      throw Error(`POST Error: ${err}`);
    }
  };
};
