import { makeGET, makePOST, baseUrl } from './memoizeFetch';

// Create memoized versions of GET & POST that load refresh_token only once.
const get = makeGET();
const post = makePOST();

// We are calling memoized get() instead of calling Fetch API directly.
export const fetchAllData = async module => {
  const endPoint = `/${module}/all`;
  const res = await get(endPoint);
  return res;
};

// We are calling memoized post() instead of calling Fetch API directly.
export const syncData = async (module, objData) => {
  const endPoint = `/${module}/add`;
  const res = await post(endPoint, objData);
  return res;
};

// Seperate SignIn as we dont need `Authorization` header here.
export const signIn = async objUser => {
  const requestUrl = `${baseUrl}/auth/login`;
  const formData = new FormData();
  Object.keys(objUser).forEach(key => {
    formData.append(key, objUser[key]);
  });

  const response = await fetch(requestUrl, {
    method: 'POST',
    headers: {
      Accept: 'application/json'
    },
    body: formData
  });

  try {
    return await response.json();
  } catch (err) {
    throw err;
  }
};
