import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import { persistStore, persistReducer, createTransform } from 'redux-persist';
import { createNetworkMiddleware } from 'react-native-offline';
import SQLite from 'react-native-sqlite-storage';
import SQLiteStorage from 'redux-persist-sqlite-storage';
import rootReducer from './rootReducer';
import { syncItem } from './modules/associate';

const storeEngine = SQLiteStorage(SQLite);
SQLite.enablePromise(true);

const actions = {
  syncItem
};

const networkMiddleware = createNetworkMiddleware();
const middlewares = [networkMiddleware, thunk];

// refer to react-native-offline
// Transform how the persistor reads the network state
const networkTransform = createTransform(
  inboundState => {
    const actionQueue = [];
    inboundState.actionQueue.forEach(action => {
      if (typeof action === 'function') {
        actionQueue.push({
          function: action.meta.name,
          args: action.meta.args
        });
      } else if (typeof action === 'object') {
        actionQueue.push(action);
      }
    });

    return {
      ...inboundState,
      actionQueue
    };
  },
  outboundState => {
    const actionQueue = [];

    outboundState.actionQueue.forEach(action => {
      if (action.function) {
        const actionFunction = actions[action.function];
        actionQueue.push(actionFunction(...action.args));
      } else {
        actionQueue.push(action);
      }
    });

    return { ...outboundState, actionQueue };
  },

  { whitelist: ['network'] }
);

const persistConfig = {
  key: 'root',
  storage: storeEngine,
  stateReconciler: autoMergeLevel2,
  transforms: [networkTransform]
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, applyMiddleware(...middlewares));

const persistor = persistStore(store);

export { persistor, store, storeEngine };
