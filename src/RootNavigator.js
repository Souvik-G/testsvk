import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Associate } from './modules/associate';

const RootNavigator = createStackNavigator(
  {
    Associate
  },
  {
    headerMode: 'none'
  }
);

export default createAppContainer(RootNavigator);
