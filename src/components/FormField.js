import React from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';
import { deviceHeightWidthMin } from '../utils/Dimensions';

const FormField = ({ label, onChangeText, value, type, err }) => (
  <View style={styles.container}>
    <TextInput
      style={styles.input}
      onChangeText={onChangeText}
      value={value}
      keyboardType={type}
      placeholder={label}
    />
    <Text style={{ height: 30 }}>{err}</Text>
  </View>
);

const styles = StyleSheet.create({
  input: {
    color: '#212121',
    height: 45,
    fontSize: 16,
    borderColor: 'grey',
    borderWidth: 1,
    paddingLeft: 10
  },
  container: {
    flex: 1
  },
  label: { fontWeight: 'bold', marginBottom: 2 }
});

export default FormField;
