import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { colorTextSecondary } from '../utils/Colors';

const NotFoundView = ({ module }) => (
  <View style={styles.container}>
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <Text bold size={18} style={styles.infoText}>
        No {module || 'record'} found
      </Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  infoText: {
    marginBottom: 20,
    color: colorTextSecondary,
    fontSize: 18
  }
});

export default NotFoundView;
