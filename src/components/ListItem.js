import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import RoundView from './RoundView';
import { colorSilverDark } from '../utils/Colors';

const ListItem = props => {
  const { primaryText, secondaryText, tertiaryText, rightElement } = props;
  return (
    <View style={styles.container}>
      <RoundView style={styles.roundView}>
        <Text style={{ fontSize: 18 }}>
          {primaryText.charAt(0).toUpperCase()}
        </Text>
      </RoundView>
      <View style={styles.content}>
        <Text style={{ color: 'black' }}>{primaryText}</Text>
        <Text numberOfLines={1} style={{ fontSize: 12 }}>
          {secondaryText}
        </Text>
        <Text style={{ fontSize: 12 }}>{tertiaryText}</Text>
      </View>
      <View style={styles.rightElement}>{rightElement}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  roundView: {
    backgroundColor: colorSilverDark
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: 10,
    paddingTop: 10,
    backgroundColor: '#ffffff'
  },
  content: { justifyContent: 'center', paddingLeft: 15 },
  rightElement: {
    marginLeft: 'auto',
    justifyContent: 'center'
  }
});

export default ListItem;
