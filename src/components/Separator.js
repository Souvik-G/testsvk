import React from 'react';
import { View, StyleSheet } from 'react-native';
import { colorSilver } from '../utils/Colors';

const Separator = props => (
  <View {...props} style={[styles.separator, props.style]} />
);

const styles = StyleSheet.create({
  separator: {
    marginLeft: 15,
    marginRight: 15,
    height: 1,
    backgroundColor: colorSilver
  }
});

export default Separator;
