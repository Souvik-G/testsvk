import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import {
  colorBorderSecondary,
  colorSecondary,
  colorWhite
} from '../utils/Colors';

const Button = ({ onPress, text, style = {} }) => (
  <View style={[styles.container, style]}>
    <TouchableOpacity onPress={onPress} style={styles.touchable}>
      <Text style={styles.buttonText}>{text}</Text>
    </TouchableOpacity>
  </View>
);

export default Button;

const styles = StyleSheet.create({
  buttonText: {
    color: colorWhite,
    fontWeight: 'bold',
    fontSize: 16
  },
  container: {
    height: 50,
    borderColor: colorBorderSecondary,
    backgroundColor: colorSecondary,
    borderRadius: 2,
    borderWidth: 2
  },
  touchable: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%'
  }
});
