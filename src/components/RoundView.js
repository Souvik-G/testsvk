import * as React from 'react';
import { View, StyleSheet } from 'react-native';

const RoundView = props => (
  <View style={[styles.round, props.style]}>{props.children}</View>
);

const styles = StyleSheet.create({
  round: {
    borderRadius: 40,
    overflow: 'hidden',
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5
  }
});

export default RoundView;
