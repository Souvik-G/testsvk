export const moduleName = 'associate';

export const addScreen = 'AddAssociate';
export const listingScreen = 'ListAssociate';

export const sampleData = {
  title: 'Mr.',
  name: '',
  phonenumber: '',
  isd_code: '+91',
  email: '',
  speciality: 'Diabetologist',
  associate_type: 'Physician',
  associate_address: '',
  associate_state: '',
  pin_code: '',
  district: ''
};

export const fields = {
  title: 'Title',
  name: 'Name',
  phonenumber: 'Phone Number',
  isd_code: 'ISD Code',
  email: 'Email',
  speciality: 'Speciality',
  associate_type: 'Type',
  associate_address: 'Address',
  associate_state: 'State',
  pin_code: 'Pin Code',
  district: 'District'
};
