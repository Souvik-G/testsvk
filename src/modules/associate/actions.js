import {
  GET_ITEMS_START,
  GET_ITEMS_SUCCESS,
  GET_ITEMS_FAILURE,
  SYNC_ITEM_START,
  SYNC_ITEM_SUCCESS,
  SYNC_ITEM_FAILURE,
  CLEAR_NEW_ITEM,
  STORE_NEW_ITEM,
  EDIT_NEW_ITEM
} from './actionTypes';

export function initGetItems() {
  return {
    type: GET_ITEMS_START
  };
}

export function okGetItems(data) {
  return {
    type: GET_ITEMS_SUCCESS,
    data
  };
}

export function errGetItems(err) {
  return {
    type: GET_ITEMS_FAILURE,
    err
  };
}

export function initSyncItem() {
  return {
    type: SYNC_ITEM_START
  };
}

export function okSyncItem(data) {
  return {
    type: SYNC_ITEM_SUCCESS,
    data
  };
}

export function errSyncItem(err) {
  return {
    type: SYNC_ITEM_FAILURE,
    err
  };
}

export function storeNewItem(data) {
  return {
    type: STORE_NEW_ITEM,
    data
  };
}

export function clearNewItem() {
  return {
    type: CLEAR_NEW_ITEM
  };
}

export function editNewItem(data) {
  return {
    type: EDIT_NEW_ITEM,
    data
  };
}
