import { combineReducers } from 'redux';
import {
  SYNC_ITEM_SUCCESS,
  CLEAR_NEW_ITEM,
  EDIT_NEW_ITEM,
  STORE_NEW_ITEM,
  GET_ITEMS_SUCCESS
} from './actionTypes';
import { sampleData } from './constants';

function data(state = [], action) {
  switch (action.type) {
    case SYNC_ITEM_SUCCESS:
      return state.map(associate => {
        if (associate.phonenumber === action.data.phonenumber) {
          return Object.assign({}, associate, {
            synced: true
          });
        }
        return associate;
      });
    case STORE_NEW_ITEM:
      return [action.data, ...state];
    case GET_ITEMS_SUCCESS:
      return action.data.map(item => ({ ...item, synced: true }));
    default:
      return state;
  }
}

function newItem(state = sampleData, action) {
  switch (action.type) {
    case CLEAR_NEW_ITEM:
      return sampleData;
    case EDIT_NEW_ITEM:
      return Object.assign({}, state, {
        [action.data.key]: action.data.value
      });
    default:
      return state;
  }
}

const associate = combineReducers({ data, newItem });

export default associate;
