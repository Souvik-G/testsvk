import { syncData, fetchAllData } from '../../api';
import {
  initSyncItem,
  okSyncItem,
  errSyncItem,
  initGetItems,
  okGetItems,
  errGetItems
} from './actions';
import { moduleName } from './constants';

export function syncItem(data) {
  function thunk(dispatch) {
    dispatch(initSyncItem());
    // Call API
    syncData(moduleName, data)
      .then(res => {
        if (res.status_code === 200) dispatch(okSyncItem(data));
      })
      .catch(err => {
        dispatch(errSyncItem(err));
      });
  }

  thunk.interceptInOffline = true; // refer to react-native-offline
  thunk.meta = {
    retry: true,
    name: 'syncItem',
    args: [data]
  };

  return thunk;
}

export function getItems() {
  async function thunk(dispatch) {
    dispatch(initGetItems());
    // Call API
    fetchAllData(moduleName)
      .then(res => {
        dispatch(okGetItems(res.data));
      })
      .catch(err => {
        dispatch(errGetItems(err));
      });
  }

  return thunk;
}
