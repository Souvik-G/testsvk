export const SYNC_ITEM_START = 'associate/SYNC_ITEM_START';
export const SYNC_ITEM_SUCCESS = 'associate/SYNC_ITEM_SUCCESS';
export const SYNC_ITEM_FAILURE = 'associate/SYNC_ITEM_FAILURE';

export const GET_ITEMS_START = 'associate/GET_ITEMS_START';
export const GET_ITEMS_SUCCESS = 'associate/GET_ITEMS_SUCCESS';
export const GET_ITEMS_FAILURE = 'associate/GET_ITEMS_FAILURE';

export const STORE_NEW_ITEM = 'associate/STORE_NEW_ITEM';
export const CLEAR_NEW_ITEM = 'associate/STORE_NEW_ITEM';
export const EDIT_NEW_ITEM = 'associate/EDIT_NEW_ITEM';
