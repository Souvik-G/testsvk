import { createSelector } from 'reselect';

const getNewItem = state => state.newItem;

const makeGetNewItem = () =>
  createSelector(
    [getNewItem],
    item => item
  );

export { makeGetNewItem };
