import React from 'react';
import { connect } from 'react-redux';
import { Text, View, ScrollView, Alert, StyleSheet } from 'react-native';
import { makeGetNewItem } from '../selector';
import { syncItem } from '../actionCreators';
import { storeNewItem, clearNewItem, editNewItem } from '../actions';
import FormField from '../../../components/FormField';
import Button from '../../../components/Button';
import { moduleName, fields } from '../constants';
import { validateInput, getKeyboardType } from '../../../utils';
import { okLogout } from '../../auth';

const AssociateAdd = ({ dispatch, itemData }) => {
  const handleSaveButtonClick = () => {
    Promise.all(
      Object.keys(itemData).map(key => validateInput(key, itemData[key]))
    )
      .then(() => {
        dispatch(storeNewItem(itemData));
        dispatch(syncItem(itemData));
        Alert.alert('Success!', `New ${moduleName} added`);
      })
      .catch(err => {
        Alert.alert('Error', err.toString());
      });
  };

  let renderContent = <View />;
  if (itemData) {
    renderContent = (
      <View>
        {Object.keys(itemData).map(key => {
          const formField = (
            <FormField
              onChangeText={text => {
                dispatch(editNewItem({ key, value: text }));
              }}
              value={itemData[key]}
              label={fields[key]}
              type={getKeyboardType(key)}
            />
          );

          return (
            <View key={key} style={styles.formFieldContainer}>
              {formField}
            </View>
          );
        })}
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.content}>
        <View style={styles.header}>
          <Text style={{ fontWeight: 'bold', fontSize: 18 }}>
            Add Associate
          </Text>
        </View>
        {renderContent}
        <View style={styles.btnContainer}>
          <Button
            text="Clear"
            style={styles.btnClear}
            onPress={() => {
              dispatch(clearNewItem());
            }}
          />
          <Button text="Add" onPress={handleSaveButtonClick} />
        </View>
        <Button
          text="Logout"
          style={styles.btnLogout}
          onPress={() => {
            dispatch(okLogout());
          }}
        />
      </ScrollView>
    </View>
  );
};

const makeMapStateToProps = () => {
  const getNewItem = makeGetNewItem();
  const mapStateToProps = state => {
    return {
      itemData: getNewItem(state[moduleName])
    };
  };
  return mapStateToProps;
};

export default connect(makeMapStateToProps)(AssociateAdd);

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#ffffff' },
  content: {
    flexGrow: 1,
    paddingLeft: 10,
    paddingRight: 10
  },
  header: {
    height: 60,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnContainer: { flexDirection: 'row', justifyContent: 'center' },
  btnLogout: {
    backgroundColor: 'grey',
    borderColor: 'darkgrey'
  },
  btnClear: {
    backgroundColor: '#f38b1f',
    borderColor: '#e0882b'
  },
  formFieldContainer: {
    flexDirection: 'row',
    paddingLeft: 10
  }
});
