import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { getItems } from '../actionCreators';
import ListItem from '../../../components/ListItem';
import Separator from '../../../components/Separator';
import NotFoundView from '../../../components/NotFoundView';
import { moduleName } from '../constants';
import { colorSecondary } from '../../../utils/Colors';

// import { setupTable } from '../../auth/actionCreators';

class ListAssociate extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    const { data } = this.props[moduleName];
    if (!data[0]) dispatch(getItems());
    // setupTable();
  }

  render() {
    const { data } = this.props[moduleName];
    let content = <NotFoundView />;
    if (data[0]) {
      content = (
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator
          ItemSeparatorComponent={() => Separator({ size: 2, spacing: 7 })}
          data={data}
          renderItem={({ item }) => (
            <ListItem
              data={item}
              primaryText={
                item.name || `${item.first_name || ''} ${item.last_name || ''}`
              }
              secondaryText={item.speciality}
              tertiaryText={`${item.isd_code || ''} ${item.phonenumber || ''}`}
              rightElement={
                item.synced ? (
                  <MaterialIcon name="sync" size={25} color={colorSecondary} />
                ) : (
                  <MaterialIcon name="sync-disabled" size={25} color="grey" />
                )
              }
            />
          )}
          ListFooterComponent={() => (
            <View style={styles.listFooter}>
              <Text>No more {moduleName}</Text>
            </View>
          )}
          contentContainerStyle={{ paddingLeft: 15, paddingRight: 15 }}
        />
      );
    }

    return <View style={{ flex: 1 }}>{content}</View>;
  }
}

const mapStateToProps = state => ({
  [moduleName]: state[moduleName]
});

export default connect(mapStateToProps)(ListAssociate);

const styles = StyleSheet.create({
  listFooter: {
    paddingTop: 20,
    paddingBottom: 100,
    alignItems: 'center'
  }
});
