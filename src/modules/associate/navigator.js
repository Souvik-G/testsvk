import { createMaterialTopTabNavigator } from 'react-navigation';
import { List, Add } from './components';

const Associate = createMaterialTopTabNavigator({
  Add,
  List
});

export default Associate;
