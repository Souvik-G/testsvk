export const moduleName = 'auth';

export const fieldsLogin = {
  username: { initialValue: '', label: 'Username' },
  password: { initialValue: '', label: 'Password' }
};

export const fieldsSignup = {
  name: { initialValue: '', label: 'Name', type: 'text' },
  username: { initialValue: '', label: 'Username', type: 'text' },
  email: { initialValue: '', label: 'Email', type: 'email' },
  password: { initialValue: '', label: 'Password', type: 'text' },
  passwordRe: { initialValue: '', label: 'Re enter password', type: 'text' },
  phone: { initialValue: '', label: 'Phone number', type: 'text' }
};
