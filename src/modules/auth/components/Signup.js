import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import Button from '../../../components/Button';
import { colorSecondary } from '../../../utils/Colors';
import { fieldsSignup } from '../constants';
import FormField from '../../../components/FormField';
import { authenticate } from '../actionCreators';

class SignupComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        name: '',
        username: '',
        email: '',
        password: '',
        passwordRe: '',
        phone: '+91'
      }
    };
  }

  handleEdit = (key, text) => {
    this.setState(prevState => ({
      user: Object.assign({}, prevState.user, { [key]: text })
    }));
  };

  handleSubmit = () => {
    const { user } = this.state;
    const { username, password } = user;
    if (!username || !password) {
      Alert.alert('Error', 'Please fill up all the fields');
      return;
    }

    this.props
      .dispatch(authenticate(user))
      .catch(err => Alert.alert('Error', err.toString()));
  };

  render() {
    const { user } = this.state;
    const content = Object.keys(user).map(key => {
      const field = fieldsSignup[key];
      const formField = (
        <FormField
          onChangeText={text => {
            this.handleEdit(key, text);
          }}
          value={user[key]}
          label={field.label}
        />
      );

      return (
        <View key={key} style={styles.formContainer}>
          {formField}
        </View>
      );
    });

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={{ fontWeight: 'bold', fontSize: 18 }}>
            Login To Your Account
          </Text>
        </View>
        {content}
        <View style={{ flexDirection: 'row', marginTop: 50 }}>
          <Button
            backgroundColor={colorSecondary}
            onPress={this.handleSubmit}
            text="Sign Up"
            style={{ flex: 1, height: 50 }}
          />
        </View>
      </View>
    );
  }
}

export default connect()(SignupComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  header: {
    padding: 20,
    alignItems: 'center'
  },
  formContainer: { flexDirection: 'row' }
});
