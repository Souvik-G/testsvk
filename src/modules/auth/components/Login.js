import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import Button from '../../../components/Button';
import { colorSecondary } from '../../../utils/Colors';
import { fieldsLogin as fields } from '../constants';
import FormField from '../../../components/FormField';
import { authenticate } from '../actionCreators';

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        username: '',
        password: ''
      }
    };
  }

  handleEdit = (key, text) => {
    this.setState(prevState => ({
      user: Object.assign({}, prevState.user, { [key]: text })
    }));
  };

  handleSubmit = () => {
    const { user } = this.state;
    const { username, password } = user;
    if (!username || !password) {
      Alert.alert('Error', 'Username and Password can not be empty');
      return;
    }

    this.props
      .dispatch(authenticate(user))
      .catch(err => Alert.alert('Error', err.toString()));
  };

  render() {
    const { user } = this.state;
    const content = Object.keys(user).map(key => {
      const field = fields[key];
      const formField = (
        <FormField
          onChangeText={text => {
            this.handleEdit(key, text);
          }}
          value={user[key]}
          label={field.label}
        />
      );

      return (
        <View key={key} style={styles.formContainer}>
          {formField}
        </View>
      );
    });

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={{ fontWeight: 'bold', fontSize: 25 }}>
            Log in to your account
          </Text>
        </View>
        {content}
        <View style={{ flexDirection: 'row', marginTop: 50 }}>
          <Button
            backgroundColor={colorSecondary}
            onPress={this.handleSubmit}
            text="Login"
            style={{ flex: 1, height: 45 }}
          />
        </View>
      </View>
    );
  }
}

export default connect()(LoginComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  header: {
    height: 100,
    alignItems: 'center',
    justifyContent: 'center'
  },
  formContainer: { flexDirection: 'row' }
});
