import React, { Component } from 'react';
import { View, UIManager, LayoutAnimation } from 'react-native';
import Button from '../../../components/Button';
import Login from './Login';
import Signup from './Signup';

const CustomLayoutAnimation = {
  duration: 500,
  create: {
    type: LayoutAnimation.Types.linear,
    property: LayoutAnimation.Properties.opacity
  },
  update: {
    property: LayoutAnimation.Properties.opacity,
    type: LayoutAnimation.Types.linear
  },
  delete: {
    duration: 300,
    property: LayoutAnimation.Properties.opacity,
    type: LayoutAnimation.Types.linear
  }
};

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: true
    };

    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  setIsLogin = value => {
    LayoutAnimation.configureNext(CustomLayoutAnimation);
    this.setState(() => ({ isLogin: value }));
  };

  render() {
    const { isLogin } = this.state;
    const component = isLogin ? <Login /> : <Signup />;
    return (
      <View style={{ flex: 1 }}>
        <View style={{ paddingLeft: 20, paddingRight: 20, flex: 1 }}>
          {component}
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Button
            text="Login"
            style={{ flex: 1 }}
            onPress={() => this.setIsLogin(true)}
          />
          <Button
            text="Signup"
            style={{ flex: 1 }}
            onPress={() => this.setIsLogin(false)}
          />
        </View>
      </View>
    );
  }
}

export default Auth;
