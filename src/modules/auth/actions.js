import {
  LOGIN_START,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS
} from './actionTypes';

export function initLogin() {
  return {
    type: LOGIN_START
  };
}

export function okLogin() {
  return {
    type: LOGIN_SUCCESS
  };
}

export function errLogin(err) {
  return {
    type: LOGIN_FAILURE,
    err
  };
}

export function okLogout() {
  return {
    type: LOGOUT_SUCCESS
  };
}
