// import { openDatabase } from 'react-native-sqlite-storage';
import { signIn } from '../../api';
import { initLogin, okLogin, okLogout } from './actions';
import { saveToken, resetToken } from '../../utils';

/* const db = openDatabase({ name: 'testsvk.db' });
export const setupTable = () => {
  db.transaction(txn => {
    txn.executeSql(
      "SELECT name FROM sqlite_master WHERE type='table' AND name='table_associate'",
      [],
      (tx, res) => {
        console.log('item:', res.rows.length);
        if (res.rows.length === 0) {
          txn.executeSql('DROP TABLE IF EXISTS table_associate', []);
          txn.executeSql(
            'CREATE TABLE IF NOT EXISTS table_associate(associate_id INTEGER PRIMARY KEY AUTOINCREMENT, associate_name VARCHAR(20), associate_phonenumber INT(10), associate_address VARCHAR(255))',
            []
          );
        }
      }
    );
  });
}; */

export function authenticate(userData) {
  async function thunk(dispatch) {
    dispatch(initLogin());
    // Call API
    return signIn(userData)
      .then(res => {
        if (res.status_code !== 200) throw Error(res.message);
        if (!res.data || !res.data.refresh_token)
          throw Error('no refresh_token');
        saveToken(res.data.refresh_token).then(dispatch(okLogin()));
      })
      .catch(err => {
        throw err;
      });
  }

  return thunk;
}

export const logout = () => dispatch => {
  resetToken().then(() => {
    dispatch(okLogout());
  });
};
