import { combineReducers } from "redux";
import { reducer as network } from "react-native-offline";
import associate from "./modules/associate";
import auth from "./modules/auth";

export default combineReducers({
  associate,
  auth,
  network
});
